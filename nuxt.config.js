const defaultTheme = require('tailwindcss/defaultTheme')

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'dtm-frontend',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: '/inter/inter.css' },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://github.com/nuxt-community/moment-module
    '@nuxtjs/moment',
  ],

  // Environment variables: https://nuxtjs.org/docs/2.x/configuration-glossary/configuration-env#automatic-injection-of-environment-variables
  env: {
    camundaCockpitUrl: 'http://localhost:8080/camunda/app/cockpit/default/#/',
  },

  // Tailwind CSS configuration: https://tailwindcss.nuxtjs.org/tailwind-config/
  tailwindcss: {
    config: {
      theme: {
        extend: {
          fontFamily: {
            sans: ['Inter var', ...defaultTheme.fontFamily.sans],
          },
          colors: {
            'needle-green': {
              50: '#faf9ef',
              100: '#f9f7cf',
              200: '#f4ef89',
              300: '#ebe042',
              400: '#d7c613',
              500: '#aea700',
              600: '#9c8504',
              700: '#796606',
              800: '#5d4d0b',
              900: '#483c0d',
            },
          },
          textColor: {
            'needle-green-text': '#8B8600',
          },
        },
      },
      plugins: [
        require('@tailwindcss/ui')({
          layout: 'sidebar',
        }),
      ],
    },
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: 'http://localhost:8081/api/', // Used as fallback if no runtime config is provided
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    hotMiddleware: {
      client: {
        overlay: false,
      },
    },
  },
}
