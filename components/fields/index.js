export { default as Boolean } from './Boolean.vue'
export { default as Integer } from './Integer.vue'
export { default as String } from './String.vue'
